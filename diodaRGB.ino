#define GREEN 1
#define BLUE 2
#define RED 3
#define delayTime 20

void setup() {

pinMode(GREEN, OUTPUT);
pinMode(BLUE, OUTPUT);
pinMode(RED, OUTPUT);

digitalWrite(GREEN, HIGH);
digitalWrite(BLUE, HIGH);
digitalWrite(RED, HIGH);
}

int redVal;
int blueVal;
int greenVal;

void loop() {

int redVal = 255;
int blueVal = 0;
int greenVal = 0;

for( int i = 0 ; i < 255 ; i ++ ){

	greenVal ++;
	redVal --;
	analogWrite(GREEN, greenVal);
	analogWrite( RED,  redVal );

	delay( delayTime );
}

redVal = 0;
blueVal = 0;
greenVal = 255;

for( int i = 0 ; i < 255 ; i += 1 ){
	blueVal += 1;
	greenVal -= 1;
	analogWrite( BLUE, blueVal );
	analogWrite( GREEN, greenVal );

	delay( delayTime );
}

redVal = 0;
blueVal = 255;
greenVal = 0;

for( int i = 0 ; i < 255 ; i += 1 ){
	redVal += 1;
	blueVal -= 1;
	analogWrite( RED,  redVal );
	analogWrite( BLUE, blueVal );

	delay( delayTime );
}

}//end loop()
